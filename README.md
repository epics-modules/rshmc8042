# rshmc8042

European Spallation Source ERIC Site-specific EPICS module: rshmc8042

Additonal information:
* [Documentation](https://confluence.esss.lu.se/pages/viewpage.action?pageId=335165388)
* [Release notes](RELEASE.md)
* [Requirements](https://gitlab.esss.lu.se/nice/staging-recipes/rshmc8042-recipe/-/blob/master/recipe/meta.yaml#L18)
* [Building and Testing](https://confluence.esss.lu.se/display/IS/1.+Development+locally+with+e3+and+Conda#id-1.Developmentlocallywithe3andConda-BuildanEPICSmoduleandtestitlocally)
