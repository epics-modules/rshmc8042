# -----------------------------------------------------------------------------
# EPICS siteMods
# -----------------------------------------------------------------------------
require stream, 2.8.10

# -----------------------------------------------------------------------------
# EPICS siteApps
# -----------------------------------------------------------------------------
require rshmc8042, 0.1.0 

# -----------------------------------------------------------------------------
# Utgard-Lab - enrivonment params
# -----------------------------------------------------------------------------
epicsEnvSet(IPADDR,   "172.30.244.139")
epicsEnvSet(IPPORT,   "5025")
epicsEnvSet(PREFIX,   "UTG-RS-HMC8042")
epicsEnvSet(PROTOCOL, "rshmc8042.proto")
# -----------------------------------------------------------------------------
# Default environment variables
# -----------------------------------------------------------------------------
epicsEnvSet("PORTNAME", "$(PREFIX)")
epicsEnvSet("IPADDR", "$(IPADDR)")
epicsEnvSet("IPPORT", "$(IPPORT)")
epicsEnvSet("PREFIX", "$(PREFIX)")
epicsEnvSet("PROTOCOL", "$(PROTOCOL)")
# Default paths to locate database and protocol
epicsEnvSet("STREAM_PROTOCOL_PATH", "$(rshmc8042_DIR)db/")
epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(rshmc8042_DIR)db/")

# -----------------------------------------------------------------------------
# Specifying the TCP endpoint and port name
# -----------------------------------------------------------------------------
drvAsynIPPortConfigure("$(PORTNAME)", "$(IPADDR):$(IPPORT)")

# -----------------------------------------------------------------------------
# Loading databases with EPICS records definition
# -----------------------------------------------------------------------------
#Load your database defining the EPICS records
dbLoadRecords("rshmc8042.db","P=$(PREFIX), PORT=$(PORTNAME), R=:, A=-1, PROTOCOL=$(PROTOCOL)")

